use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    println!("Number Guessing Game!");
    let secret_number: u32 = gen_secret();

    loop {
        println!("Enter the number:");

        let mut guess  = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line!");       

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}

fn gen_secret() -> u32 {
    let secret = rand::thread_rng().gen_range(1..101);
    if secret % 5 == 0{
        gen_secret()
    } else {
        return secret;
    }
}
